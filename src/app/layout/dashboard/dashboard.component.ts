import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../router.animations';

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.scss'],
    animations: [routerTransition()]
})
export class DashboardComponent implements OnInit {
    public alerts: Array<any> = [];
    public sliders: Array<any> = [];

    constructor() {
        this.sliders.push(
            {
                imagePath: 'assets/images/prop1.jpg',
                label: '',
               text: 'Seja Bem Vindo ao Dreliver!'
                    
            },
            {
                imagePath: 'assets/images/prop3.jpg',
                label: '',
                text: 'Promoção pra tomar aquela cervejinha gelada'
            },
            {
                imagePath: 'assets/images/prop4.jpg',
                label: '',
                text:'Cervejas em promoção'
                    
            }
        );

        this.alerts.push(
            {
                id: 1,
                type: 'success',
                message: `TESTANDO`
            },
            {
                id: 2,
                type: 'warning',
                message: `TESTANDO 2`
            }
        );
    }

    ngOnInit() {}

    public closeAlert(alert: any) {
        const index: number = this.alerts.indexOf(alert);
        this.alerts.splice(index, 1);
    }
}
