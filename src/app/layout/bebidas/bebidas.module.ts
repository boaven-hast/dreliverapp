import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { BebidasRoutingModule } from './bebidas-routing.module';
import { BebidasComponent } from './bebidas.component';
import { PageHeaderModule } from '../../shared';
import {
  ModalComponent,
  CollapseComponent,
  RatingComponent,
} from './components';

@NgModule({
  imports: [
    CommonModule,BebidasRoutingModule,FormsModule,PageHeaderModule,ReactiveFormsModule,NgbModule.forRoot()],
  declarations: [BebidasComponent,CollapseComponent,RatingComponent,ModalComponent]
})
export class BebidasModule { }
