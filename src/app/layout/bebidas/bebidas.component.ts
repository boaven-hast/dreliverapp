import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../router.animations';

@Component({
  selector: 'app-bebidas',
  templateUrl: './bebidas.component.html',
  styleUrls: ['./bebidas.component.scss'],
  animations: [routerTransition()]
})
export class BebidasComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
