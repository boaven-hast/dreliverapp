import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../router.animations';

@Component({
  selector: 'app-ajuda',
  templateUrl: './ajuda.component.html',
  styleUrls: ['./ajuda.component.scss'],
  animations: [routerTransition()]
})
export class AjudaComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
