import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AjudaRoutingModule } from './ajuda-routing.module';
import { AjudaComponent } from './ajuda.component';
import { PageHeaderModule } from '../../shared';
import {CollapseComponent, ModalComponent} from './component';

@NgModule({
  imports: [
    CommonModule,
    AjudaRoutingModule,
    PageHeaderModule,
    ReactiveFormsModule,
        NgbModule.forRoot(),
  ],
  declarations: [AjudaComponent, ModalComponent,CollapseComponent]
})
export class AjudaModule { }
