import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from './layout.component';

const routes: Routes = [
    {
        path: '',
        component: LayoutComponent,
        children: [
            { path: '', redirectTo: 'paginainicial', pathMatch: 'prefix' },
            { path: 'favoritos', loadChildren: './favoritos/favoritos.module#FavoritosModule' },
            { path: 'paginainicial', loadChildren: './paginainicial/paginainicial.module#PaginainicialModule' },
            { path: 'bebidas', loadChildren: './bebidas/bebidas.module#BebidasModule' },
            { path: 'estabelecimentos', loadChildren: './estabelecimentos/estabelecimentos.module#EstabelecimentosModule' },
            { path: 'ajuda', loadChildren: './ajuda/ajuda.module#AjudaModule' }
            
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class LayoutRoutingModule {}
