import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../router.animations';


@Component({
  selector: 'app-paginainicial',
  templateUrl: './paginainicial.component.html',
  styleUrls: ['./paginainicial.component.scss'],
  animations: [routerTransition()]
})
export class PaginainicialComponent implements OnInit {
    public alerts: Array<any> = [];
    public sliders: Array<any> = [];

  constructor() { 
      this.sliders.push(
        {
          imagePath: 'assets/images/prop1.jpg',
          label: '',
         text: 'Seja Bem Vindo ao Dreliver!'
              
      },
      {
          imagePath: 'assets/images/prop3.jpg',
          label: '',
          text: 'Promoção pra tomar aquela cervejinha gelada'
      },
      {
          imagePath: 'assets/images/prop4.jpg',
          label: '',
          text:'Cervejas em promoção'
              
      }
    );
      
}

  ngOnInit() {}
    public closeAlert(alert: any) {
      const index: number = this.alerts.indexOf(alert);
      this.alerts.splice(index, 1);
  }

}
