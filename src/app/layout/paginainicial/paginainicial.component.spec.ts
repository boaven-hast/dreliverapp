import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'

import { PaginainicialComponent } from './paginainicial.component';
import { PaginainicialModule } from './paginainicial.module';

describe('PaginainicialComponent', () => {
  let component: PaginainicialComponent;
  let fixture: ComponentFixture<PaginainicialComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        PaginainicialModule,
        RouterTestingModule,
        BrowserAnimationsModule,
      ]
    })
    .compileComponents();
  })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(PaginainicialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
