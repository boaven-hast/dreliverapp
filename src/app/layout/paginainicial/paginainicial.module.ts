import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbCarouselModule, NgbAlertModule } from '@ng-bootstrap/ng-bootstrap';

import { PaginainicialRoutingModule } from './paginainicial-routing.module';
import { PaginainicialComponent } from './paginainicial.component';
import {
  TimelineComponent,
  NotificationComponent,
  ChatComponent,
  
} from './components';
import { StatModule } from '../../shared';

@NgModule({
  imports: [
    CommonModule, 
    NgbCarouselModule.forRoot(),
    NgbAlertModule.forRoot(),
    PaginainicialRoutingModule,
   StatModule
  ],
  declarations: [
    PaginainicialComponent,
    TimelineComponent,
    NotificationComponent,
    ChatComponent,
  ]
})
export class PaginainicialModule { }
