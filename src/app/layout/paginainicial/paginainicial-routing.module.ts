import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PaginainicialComponent } from './paginainicial.component';

const routes: Routes = [
  {
      path: '', component: PaginainicialComponent
  }
];



@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
  
})
export class PaginainicialRoutingModule { }
