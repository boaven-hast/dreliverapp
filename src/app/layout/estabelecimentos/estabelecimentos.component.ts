import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../router.animations';

@Component({
  selector: 'app-estabelecimentos',
  templateUrl: './estabelecimentos.component.html',
  styleUrls: ['./estabelecimentos.component.scss'],
  animations: [routerTransition()]
})
export class EstabelecimentosComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
