import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { EstabelecimentosRoutingModule } from './estabelecimentos-routing.module';
import { EstabelecimentosComponent } from './estabelecimentos.component';
import { PageHeaderModule } from '../../shared';
import {
  ModalComponent,
  CollapseComponent,
  RatingComponent,
} from './components';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    EstabelecimentosRoutingModule,
    PageHeaderModule,
    ReactiveFormsModule,NgbModule.forRoot()
  ],
  declarations: [EstabelecimentosComponent,CollapseComponent,RatingComponent,ModalComponent]
})
export class EstabelecimentosModule { }
